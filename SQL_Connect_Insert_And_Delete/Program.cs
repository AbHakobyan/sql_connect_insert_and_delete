﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQL_Connect_Insert_And_Delete
{
    class Program
    {
        static void Main(string[] args)
        {
            string conStr = @"Data Source =.\SQLEXPRESS; Initial Catalog = ShopDB; Integrated Security = true;";
            SqlConnection connection = new SqlConnection(conStr);

            //Open SQL Connection
            connection.Open();
            //Created SQL Command name InsertCommand
            SqlCommand InsertCommand = connection.CreateCommand();
            //Create Command Text 
            InsertCommand.CommandText = "INSERT Custumers VALUES('Albert','Hakobyan','Razmiki','Hoktember 1','Armavir','(91)342235','2018/07/10','Armenia')";

            //With this command, we can use the Insert, Delete, Update command transfer fields
            int rowAffected = InsertCommand.ExecuteNonQuery();

            //Print lines 
            Console.WriteLine("INSERT command rows affected" + rowAffected);

            //Create SQL Command name DeleteCommand
            SqlCommand DeleteCommand = connection.CreateCommand();

            //Create CommanText
            DeleteCommand.CommandText = "DELETE Custumers WHERE DataWork = '(91)342235'";

            //With this command, we can use the Insert, Delete, Update command transfer fields
            rowAffected = DeleteCommand.ExecuteNonQuery();

            //Print lines
            Console.WriteLine("DELETE command rows affected" + rowAffected);

            //Close SQL Connection
            connection.Close();
        }
    }
}
